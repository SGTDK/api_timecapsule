var server = require('./server/server');
var ds = server.dataSources.mongoDS;
var lbTables = ['Image', 'User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'Client'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  //ds.disconnect();
});

