'use strict';

let app = require('../../server/server');

module.exports = function(Client) {
  delete Client.validations.email; // Todo (Dette er "send email og verify før login) Fix email validation before live, fix ogå client.json sådan at parametere er "required", skal være unique
  delete Client.validations.password; // Todo Skal være enabled før live
  delete Client.validations.username;// Todo Skal være enabled før live, skal være unique

  // Todo Der skal laves hooks sådan at hvis Dropbox information (ID/AccessToken) ændres, skal alle billeder slettes og der skal re-scannes

  Client.getImagesFromDate = async function(date) {
    let Image = app.models.Image;
    const wait = Image.getImagesFromDate(date).then((imageArray) => {
      return imageArray;
    });
    return wait;
  };



  Client.testMethod = async function(someArgument) {
    console.log('TestMethod hit with someArgument: ', someArgument);
    let someReturn = ['The Response Array'];
    console.log('TestMethod will respond with someReturn: ', someReturn);
    return someReturn;
  };


  Client.observe('after save', async function(ctx) {
    return;
  });


  Client.dropboxCallback = function(dropboxData, callback) {
    console.log(' DropboxCallback method hit ');
    callback(null);
  };


    Client.updateImages = function(clientId, callback) { // Todo Der skal først sendes svar når alle er opdateret. Evt. med Async/await/response i stedet for callback
      let Image = app.models.Image;
      Client.findById(clientId, function(err, client) {
        if (err) {
          return callback(err);
        } else {
          Image.deleteImages(client).then((err)=> {
            if (err) {
              console.log('ERROR_Client.updateImages: ', err);
            } else
            {
              Image.updateImages(client)
            }
          });
          let response = [200];
          callback(null, response);
        }
      });
    };


  /*
    Client.updateImages = (clientId) => {
      let Image = app.models.Image;
      let myUpdate = Client.findById(clientId, function(err, client) {
        if (err) {
          return (err);
        } else {
          Image.deleteImages(client).then((err)=> {
            if (err) {
              console.log('ERROR_Client.updateImages: ', err);
            } else
            {
              Image.updateImages(client)
            }
          });
        }
      });
      return ({
        status:200,
        contents: myUpdate
      });
    };

  */


};
