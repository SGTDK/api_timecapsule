'use strict';

module.exports = function(Cloudprovider) {

  Cloudprovider.observe('access', function logQuery(ctx, next) {
    // console.log('CloudProvider) Accessing %s', ctx.Model.modelName);
    // console.log('CloudProvider) StatusCode: ', ctx.statusCode);
    next();
  });

};
