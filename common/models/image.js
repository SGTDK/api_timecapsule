'use strict';
var Dropbox = require('dropbox').Dropbox;
var moment = require('moment');

// Todo Make sure images are unique, for hver bruger,  before saving. Husk at det samme billede kan godt være his flere brugere
// Todo Der skal laves operational URL-hooks på dropbox brugere, sådan at databasen kan blive opdateret med ændringer hos brugerne


module.exports = function(Image) {

  let totalImages = 0;
  let totalDefectImages = 0;


  Image.getImagesFromDate = async function(dateTaken) {
    if (typeof dateTaken !== 'string') {
      dateTaken = moment();
    }
    let month = moment(dateTaken).format('MM');
    let day = moment(dateTaken).format('DD');
    const wait = await Promise.all([
      Image.find({
        where: {monthTaken: month, dayTaken: day},
        order: 'unixTaken',
      }).then((ImageArray) => {
        let numberOfImages = ImageArray.length;
        console.log('     There were', numberOfImages, 'images from month: ', month,'   day: ', day);
        return ImageArray;
      }),
    ]);
    return wait;
  };


  Image.deleteImages = async function(client) {
    Image.destroyAll({clientId: client.id}, function(err, object, number) {
      console.log('Images deleted before adding new: ', object.count);
      if (err) {
        console.log('ERROR_Image.deleteImages: ', Image.deleteImages(client));
      }
      return (object.count);
    });
  };


  Image.updateImages = function(client) {
    if (client) {
      Image.getDropboxFiles(client).then(() => {
        // todo Der er noget galt med return/await kæden. Dette trigger før det burde, tror det er skiftet tli "getMoreDropboxFiles"
        console.log('updateImages: Total Images Added: ', totalImages);
        console.log('updateImages: Total files not Added: ', totalDefectImages);
        return totalImages;
      });
    }
  };


  Image.getMoreDropboxFiles = async function(cursor, client) {
    var dbx = new Dropbox({
      accessToken: client.dropboxAccessToken,
      fetch: fetch,
    });
    await dbx.filesListFolderContinue({
      cursor: cursor,
    }).then(function(response) {
      updateTable1(response, client);
      if (response.has_more) {
        Image.getMoreDropboxFiles(response.cursor, client);
      } else {
        console.log(' No more images ');
        console.log('getMoreDropboxFiles: Total Images Added: ', totalImages);
        console.log('getMoreDropboxFiles: Total files not Added: ', totalDefectImages);
        return null;
      }
    })
      .catch(function(error) {
        console.log(error);
      });
  };

  Image.getDropboxFiles = async function(client) {
    var dbx = new Dropbox({
      accessToken: client.dropboxAccessToken,
      fetch: fetch,
    });
    await dbx.filesListFolder({
      path: '',
      recursive: true,  // skal være "true" for at få alle billeder
      include_media_info: true,
    }).then(function(response) {
      updateTable1(response, client);
      if (response.has_more) {
        console.log('  (getDropboxFiles) hasMore: ', response.has_more);
        Image.getMoreDropboxFiles(response.cursor, client).then(() => {
          console.log('1getDropboxFiles: Total Images Added: ', totalImages);
          console.log('1getDropboxFiles: Total files not Added: ', totalDefectImages);
          return null;
        });
      } else {
        console.log('2getDropboxFiles: Total Images Added: ', totalImages);
        console.log('2getDropboxFiles: Total files not Added: ', totalDefectImages);
        return null;

      }
    })
      .catch(function(error) {
        console.log(error);
      });
    return null;
  };

  const updateTable1 = function(response, client) {
    let countImages = 0;
    let countDefectImages = 0;
    for (var i = 0; i < response.entries.length; i++) {
      if (response.entries[i]['media_info']) {
        let mediaInfo = response.entries[i]['media_info'];
        if (mediaInfo['metadata']['time_taken']) {
          let saveImage = new Image;
          saveImage.clientId = client.id;
          saveImage.name = response.entries[i].name;
          saveImage.imageId = response.entries[i].id;
          saveImage.size = response.entries[i].size;
          saveImage.timeTaken = response.entries[i].media_info.metadata.time_taken;
          saveImage.dateTaken = moment(saveImage.timeTaken).format('YYYY-MM-DD');
          saveImage.monthTaken = moment(saveImage.timeTaken).format('MM');
          saveImage.yearTaken = moment(saveImage.timeTaken).format('YYYY');
          saveImage.dayTaken = moment(saveImage.timeTaken).format('DD');
          saveImage.unixTaken = moment(saveImage.timeTaken).format('X');
          saveImage.pathDisplay = response.entries[i].path_display;
          Image.create(saveImage);
          countImages++;
          totalImages++;
        } else {
          //console.log('Missing time_taken: File name: ', response.entries[i].name, '    media_info: ',response.entries[i]['media_info']['metadata']['.tag'] );
          if (response.entries[i]['media_info']['metadata']['.tag'] === 'photo') {
            countDefectImages++;
            totalDefectImages++;
          }
        }
      }
    }
    console.log('Number of images written: ', countImages);
    console.log('Number of defect photos not saved: ', countDefectImages);
  };
};
