# My Application

Must do:
#Todo fra filerne
Ved nye bruger begynder skanning inden der er kommet token fra dropbox i DB
Setup Prod vs Dev config (disable /explore in live version)
Når en bruger slettes skal alle billeder og dropbox slettes
SSL-certifikat/Dropbox setup
Når dropbox scannes tager det tid, men der sendes reseponse to APP så snart kald er modtaget. Noget galt med "return"
Der er noget galt med CORS-access (Check npm CORS)
Der skal laves hoocks til dropbox, sådan at ændringer i dropbox reflekteres i databasen


Should do:
Tælleren af dropbox billeder nul-stilles ikke, hvis der ikke kommer nogle billeder ind
Error handling
Input sanitazation
Access rights
Development vs Production env
Lav "default" i funktions call:   var logRequestBody = options.hasOwnProperty('logRequestBody') ? options.logRequestBody : true;
Når imageTaken date fastæsttes, så screen for den ældste værdi i alle parameter fra Dropbox, fx. filename, ændringsdato, exif-data osv
Flyt alt logic til backend
operational hooks på dropbox, i stedet for manuel re-load


Can do:
OneDrive, iCloud integration
Backend support
Runtime optimization
Hvis dropbox exif-data mangler men det er et billede, så brug evt. filenavnet, ændringsdato eller andet.
Lav mulighed for at styre server-information/settings , ved fx. "Varriables" eller API calls



 "mongoDS": {
    "host": "ds161224.mlab.com",
    "port": 61224,
    "url": "mongodb://apiTimecapsule:Database1234@ds161224.mlab.com:61224/heroku_w66t9wq4",
    "database": "apiTimecapsule",
    "password": "Database1234",
    "name": "mongoDS",
    "user": "apiTimecapsule",
    "connector": "mongodb"
  }


vs

"mongoDS": {
    "host": "localhost",
    "port": 27017,
    "url": "",
    "database": "apiTimecapsule",
    "password": "",
    "name": "mongoDS",
    "user": "",
    "connector": "mongodb"
  }
